package com.`as`.browserapp

import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.`as`.browserapp.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var actionBarTitle : String? = "Browser App"

    private var btnColor = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState==null){
            btnColor =
                if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) R.color.brown
                else Color.BLUE
            actionBarTitle = "Browser app"
        }
        else {
            btnColor=savedInstanceState.getInt("KEY_COLOR");
            actionBarTitle = savedInstanceState.getString("KEY_TITLE");
            binding.webView.restoreState(savedInstanceState.getBundle("webViewState")!!)
        }

        val actionBar = supportActionBar
        actionBar!!.title = actionBarTitle

        binding.button.setBackgroundColor(btnColor)

        binding.webView.webViewClient = object: WebViewClient(){
            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                view.loadUrl(request.url.toString())
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                actionBarTitle = if (binding.webView.title.isNullOrEmpty()) {
                    getString(R.string.app_name)
                    } else {
                        binding.webView.title
                    }
                actionBar.title = actionBarTitle
            }
        }

        binding.webView.settings.javaScriptEnabled = true

        binding.button.setOnClickListener {
            val url = binding.editText.text.toString()
            binding.webView.loadUrl(url)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("KEY_COLOR", btnColor)
        outState.putString("KEY_TITLE", actionBarTitle)
        val bundle = Bundle();
        binding.webView.saveState(bundle);
        outState.putBundle("webViewState", bundle)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
            menuInflater.inflate(R.menu.menu_main_landscape, menu)
        else menuInflater.inflate(R.menu.menu_main_portrait, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        btnColor = when (item.itemId) {
            R.id.green -> Color.GREEN
            R.id.blue -> Color.BLUE
            R.id.violet -> resources.getColor(R.color.purple_500)
            R.id.brown -> resources.getColor(R.color.brown)
            R.id.yellow -> Color.YELLOW
            else -> resources.getColor(R.color.orange)
        }
        binding.button.setBackgroundColor(btnColor)
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (binding.webView.canGoBack()) {
            binding.webView.goBack()
        } else {
            super.onBackPressed()
        }
    }
}